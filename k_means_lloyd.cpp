#include <cstdlib>
#include <iostream>
#include <limits>
#include "k_means_lloyd.hpp"
#ifdef _OPENMP
#include <omp.h>
#endif

KMeansLloyd::KMeansLloyd(int num_clusters, std::vector<Point> &point_list,
                         int max_iter)
    : points(point_list),
      mean_id_of_point(point_list.size(), 0),
      num_points_in_cluster(num_clusters, 0) {
    clusters_count = num_clusters;
    points_count = point_list.size();
    max_iterations = max_iter;
    num_points_in_cluster[0] = points_count;
    InitializeMeans();
}

int KMeansLloyd::Compute() {
    int cur_iter = 0;
    while (cur_iter < max_iterations) {
        bool means_changed = false;
#pragma omp parallel
#pragma omp for nowait
        for (int i = 0; i < points_count; i++) {
            int old_mean_id = mean_id_of_point[i];
            int new_mean_id = NearestMean(points[i]);
            if (old_mean_id != new_mean_id) {
                means_changed = true;
                mean_id_of_point[i] = new_mean_id;
#pragma omp atomic update
                num_points_in_cluster[old_mean_id]--;
#pragma omp atomic update
                num_points_in_cluster[new_mean_id]++;
            }
        }
        if (!means_changed) {
            return cur_iter;
        }
        std::vector<float> sum_x(clusters_count, 0);
        std::vector<float> sum_y(clusters_count, 0);
        for (int i = 0; i < points_count; i++) {
            int cluster_id = mean_id_of_point[i];
            sum_x[cluster_id] += points[i].first;
            sum_y[cluster_id] += points[i].second;
        }
        for (int i = 0; i < clusters_count; i++) {
            means[i] = std::make_pair(sum_x[i] / num_points_in_cluster[i],
                                      sum_y[i] / num_points_in_cluster[i]);
        }
        cur_iter++;
    }
    return cur_iter;
}

void KMeansLloyd::PrintMeansWithPointCount(std::ofstream &file) {
    for (int i = 0; i < clusters_count; i++) {
        file << i << "\t" << means[i].first << "\t" << means[i].second << "\t"
             << num_points_in_cluster[i] << std::endl;
    }
}

void KMeansLloyd::PrintPointsWithMeanId(std::ofstream &file) {
    for (int i = 0; i < points_count; i++) {
        file << points[i].first << "\t" << points[i].second << "\t"
             << mean_id_of_point[i] << std::endl;
    }
}

void KMeansLloyd::InitializeMeans() {
    int remaining_points_count = points_count;
    means.reserve(clusters_count);
    auto begin_iter = points.begin();
    // Fisher-Yates shuffle
    for (int i = 0; i < clusters_count; i++) {
        auto iter_to_random_pt = begin_iter;
        std::advance(iter_to_random_pt, std::rand() % remaining_points_count);
        means.push_back(*iter_to_random_pt);
        std::swap(*begin_iter, *iter_to_random_pt);
        ++begin_iter;
        remaining_points_count--;
    }
}

int KMeansLloyd::NearestMean(Point &point) {
    int nearest_mean_id = -1;
    float nearest_mean_dist_sqr = std::numeric_limits<float>::max();
    for (int i = 0; i < clusters_count; i++) {
        float dist_sqr = L2DistanceSquared(point, means[i]);
        if (dist_sqr < nearest_mean_dist_sqr) {
            nearest_mean_id = i;
            nearest_mean_dist_sqr = dist_sqr;
        }
    }
    return nearest_mean_id;
}

float KMeansLloyd::L2DistanceSquared(Point &point_1, Point &point_2) {
    float x_diff = point_1.first - point_2.first;
    float y_diff = point_1.second - point_2.second;
    return x_diff * x_diff + y_diff * y_diff;
}
