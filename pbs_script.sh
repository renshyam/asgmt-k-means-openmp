#!/bin/bash
#PBS -N myjob
#PBS -l nodes=1:ppn=8

cd /home/rengak/asgmt-k-means-openmp
mkdir -p output
for n in 1 2 3 4 5 ; do \
    ./run.out -i data/data_500k.csv -o output/output.txt \
        -r output/result.txt -t output/time_${n}.txt ; \
done
for s in STATIC DYNAMIC ; do \
    export OMP_SCHEDULE="${s}"; \
    for t in 2 4 8 16 ; do \
        export OMP_NUM_THREADS=${t} ; \
        for n in 1 2 3 4 5 ; do \
            ./run_omp.out -i data/data_500k.csv -o output/output_omp.txt \
                -r output/result_omp.txt \
                -t output/time_omp_${s}_${t}_${n}.txt ; \
        done \
    done \
done
