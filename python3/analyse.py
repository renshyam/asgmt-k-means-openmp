import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt

SEQ_TIME_FILE_FORMAT = "output/time_%d.txt"
OMP_TIME_FILE_FORMAT = "output/time_omp_%s_%d_%d.txt"

SHEDULE_TYPES = ["STATIC", "DYNAMIC"]
THREAD_COUNT = [2, 4, 8, 16]

TIME_PLOT_PATH = "results/times.png"
SPEEDUP_PLOT_PATH = "results/speedup.png"


def get_time_from_file(file_path):
    with open(file_path) as f:
        return int(next(f))


if __name__ == "__main__":
    sum = 0
    for i in range(1, 6):
        sum += get_time_from_file(SEQ_TIME_FILE_FORMAT % i)
    seq_avg_time = sum / 5
    print("Avg time for sequential K-Means(ms): %f" % seq_avg_time)
    avg_time = {}
    avg_time["STATIC"] = []
    avg_time["DYNAMIC"] = []
    for s in SHEDULE_TYPES:
        for t in THREAD_COUNT:
            sum = 0
            for i in range(1, 6):
                sum += get_time_from_file(OMP_TIME_FILE_FORMAT % (s, t, i))
            avg_time[s].append(sum / 5)
            print("Avg.time for ||el k-means using %d threads" \
                  " in %s schedule(ms): %f" % (t, s, avg_time[s][-1]))
    plt.figure()
    plt.plot(
        THREAD_COUNT,
        avg_time["STATIC"],
        '-b',
        label="Parallel K-Means(Static shedule)")
    plt.plot(
        THREAD_COUNT,
        avg_time["DYNAMIC"],
        '-g',
        label="Parallel K-Means(Dynamic shedule)")
    plt.axhline(
        y=seq_avg_time,
        color='r',
        linestyle='--',
        label="Sequential K-means (independent of X-axis)")
    plt.legend(loc="best")
    plt.title("Running time vs Thread Count")
    plt.xlabel("Number of OpenMP threads")
    plt.ylabel("Time taken (milliSeconds)")
    plt.savefig(TIME_PLOT_PATH)
    static_speedups = [seq_avg_time / x for x in avg_time["STATIC"]]
    dynamic_speedups = [seq_avg_time / x for x in avg_time["DYNAMIC"]]
    plt.figure()
    plt.plot(
        THREAD_COUNT,
        static_speedups,
        '-b',
        label="Speedup using Static shedule")
    plt.plot(
        THREAD_COUNT,
        dynamic_speedups,
        '-g',
        label="Speedup using Dynamic shedule")

    plt.legend(loc="lower right")
    plt.title("Speedup vs Thread Count")
    plt.xlabel("Number of threads")
    plt.ylabel("Speedup")
    plt.savefig(SPEEDUP_PLOT_PATH)
