import pandas as pd
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt

OUTPUT_FILES = ["output/output.txt", "output/output_omp.txt"]
RESULT_FILES = ["output/result.txt", "output/result_omp.txt"]
IMAGE_FILES = ["results/scatter_plot.png", "results/scatter_plot_omp.png"]


def plot_clusters(output_file_path, result_file_path, image_save_path):
    points_with_cluster_ids = pd.read_csv(
        output_file_path, sep="\t", header=None).values
    clusters = pd.read_csv(
        result_file_path, sep="\t", header=None).values[:, [1, 2]]
    plt.figure()
    plt.scatter(
        points_with_cluster_ids[:, 0],
        points_with_cluster_ids[:, 1],
        c=points_with_cluster_ids[:, 2],
        cmap="rainbow",
        alpha=0.5)
    plt.scatter(clusters[:, 0], clusters[:, 1], marker="^", c="black")
    plt.savefig(image_save_path)
    plt.close()


if __name__ == "__main__":
    for ofp, rfp, isp in zip(OUTPUT_FILES, RESULT_FILES, IMAGE_FILES):
        plot_clusters(ofp, rfp, isp)
